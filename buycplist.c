#include"buycplist.h"


buy *mkbuyNode()//创建购买彩票空节点
{
	buy *node=(buy*)malloc(sizeof(buy));
	if(NULL==node)
	{
		exit(EXIT_FAILURE);
	}
	memset(node, 0,sizeof(buy));
	return node;
}

blist *mkbuylist()//创建头节点
{
	blist *list=(blist*)malloc(sizeof(blist));
	if(NULL==list)
	{
		exit(EXIT_FAILURE);
	}
	memset(list, 0,sizeof(blist));
	return list;
}

FILE *openfile(const char *path,const char *mode)//打开文件
{
	FILE *file=NULL;
	if(NULL!=path && NULL!=mode)
	{
		file=fopen(path,mode);
		if(NULL==file)
		{
			printf("open %s failed->%s\n",path,strerror(errno));
			exit(EXIT_FAILURE);
		}
	}
	else
	{
		printf("参数不对\n");
		exit(EXIT_FAILURE);
	}
	return file;
}

void loadBuy(const char *path,blist *list)//加载
{
	if(NULL==path || NULL==list)
	{
		return;
	}
	FILE *file=openFile(path,"rb");
	buy *node=NULL;
	int ret=0;
	while(1)
	{
		node=mkbuyNode();
		ret=fread(node,sizeof(buy), 1,file);
		if(0==ret)
		{
			free(node);
			node=NULL;
			if(feof(file))
			{
				break;
			}
			else if(ferror(file)
			{
				fclose(file);
				printf("read file error...\n");
				exit(EXIT_FAILURE);
			}
		}
		node->next=NULL;
		insert(list,node);////////
	}
	fclose(file);
}

void saveBuy(const char *path,const blist *list1)//保存
{
	if(NULL==path || NULL==list)
	{
		return;
	}
	FILE *file=openFile(path,"wb");
	buy *node=list->pFirstNode;
	while(NULL!=node)
	{
		printf("issueNum:%d,id:%s,winNum:%s,name:%s,total_buy:%d,winSta:%s,prize_win:%lf\n",node->issueNum,node->id,node->winNum,node->name,node->total_buy,node->winSta,node->prize_win);
		fwrite(node,sizeof(buy), 1,file);
		node=node->next;
	}
	fclose(file);
}

void insertBuy(blist *list,buy *node)//头插
{
	if(NULL==list || NULL==node)
	{
		return;
	}
	node->next=list->pFirstNode;
	list->pFirstNode=node;
}

	
//path usr  /path1 buy /path2 fa
void buyTic(const char *path,const char *path1,const char *path2)//买票/期号/id/
{
	if(NULL==path || NULL==path1 || NUll==path2 || NULL==usr)
	{
		return;
	}
	else
	{
		buy *node=mkbuyNode();//生成个人彩票节点
		blist *list1=mklist();//生成个人彩票的头
		while(1)//购买彩票号码
		{
			getchar();
			printf("输入购买的彩票号码:\n");
			gets(node->winNum);//读取彩票号码
			if(strlen(node->winNum)==7)//计算彩票号码的长度
			{
				break;
			}
			else
			{
				printf("请输入正确彩票位数号码\n");
			}
		}
		printf("输入购买注数:\n");
		scanf("%d",&total_buy);
		flist *list2=mklist();;//生成发行彩票的头
		loadIss(path2,list2);//加载每期彩票链表，头节点的next是最新一期彩票
		pnode->issueNum=list2->pFirstNode->issueNum;//将期号给节点//////////
		signNumber(node->id);//生成彩票标识号/随机数

		List *list=mkList();//创建用户头
		load(list,path);//加载用户信息
		usr *unode=list->pFirstNode;
		findusrByName(name,list);
		unode->money=unode->money - list2->pFirstNode->unit_price * node->total_buy;
		if(unode->money < 0)
		{
			printf("余额不足\n");
		}
		else
		{
			printf("购买成功\n");
		}
		insertBuy(list1,node);
		saveBuy(list1,path1);//保存到文件
		save(list,path);
		saveIss(list2,path2);
		//根据注数生成彩票标识号码个数
		//根据注数生成循环打印次数
	}
}

void signNumber(char a[])//随机数
{
	char array[10]="0123456789";
	while(1)
	{
		srand(time(NULL));
		a[0]=array[rand() % 10];
		a[1]=array[rand() % 10];
		a[2]=array[rand() % 10];
		a[3]=array[rand() % 10];
		a[4]=array[rand() % 10];
		a[5]=array[rand() % 10];
		a[6]=array[rand() % 10];
		break;
	}
}

void findwinJl(const char *path)//查询中奖记录
{
	blist *list=mklist();
	loadBuy(path,list);
	buy *node=list->pFirstNode;
	char *win="win";
	while(NULL!=node)
	{
		if(strcmp(node->winSta,win)==0)
		{
			printf("中奖号码\t中奖标识码\t注数\t中奖状态\t中奖金额\n");
			scanf("%s\t%s\t%d\t%s\t%lf",node->winNum,node->id,node->total_buy,node->winSta,node->prize_win);
		}
		else
		{
			node=node->next;
		}
	}
}

//path usr /path1 buy/ path2 fa
void lottery(const char *path1,const char *path2)//开奖
{
	if(NULL==path || NULL==path1)
	{
		exit(EXIT_FAILURE);
	}
	printf("1,手动开奖\n");
	printf("2,自动开奖\n");
	printf("0,退出\n");
	int choice;
	printf("输入选择:\n");
	scanf("%d",&choice);
	flist *list2=mklist();//创建发行彩票的头
	loadIss(path2,list2);
	iss *node=list->pFirstNode;//加载链表/pFirstNode是最新一期彩票/开奖
	if(choice>0 && choice<9)
	{
		switch(choice)
		{
			case 1://手动开奖
				while(1)
				{
					getchar();
					printf("输入本期中奖号码:\n");
					gets(node->winNum);
					 if(strcmp(node->winNum)==7)
					{
						break;
					}
				}
				char *award="award";//初始化开奖状态
				strcpy(node->lotterySta,sward);//开奖
				blist *list1=mklist();
				loadBuy(list1,path1);
				buy *pnode=list1->pFirstNode;//遍历个人彩票 求中奖注数

				int totalWin=0;///初始化中奖总注数
				while(pnode!=NULL)
				{
					if(strcmp(pnode->winNum,node->winNum)==0
					{
						totalWin = totalWin + pnode->total_buy;
					}
				}
				int one_prize=0;//每注中奖金额
				one_prize = node->prize_pool / node->total_sold;
				saveIss(path2,list2);//////////////////////////////////////
				printf("开奖成功\n");
				getchar();
				printf("回车继续\n");
				getchar();
				break;
			case 2://自动开奖 
				autoLottery(path2);
				blist *list1=mklist();
				loadBuy(list1,path1);
				buy *pnode=list1->pFirstNode;//遍历个人彩票 求中奖注数

				int totalWin=0;///初始化中奖总注数
				while(pnode!=NULL)
				{
					if(strcmp(pnode->winNum,node->winNum)==0
					{
						totalWin = totalWin + pnode->total_buy;
					}
				}
				int one_prize=0;//每注中奖金额
				one_prize = node->prize_pool / node->total_sold;
				saveIss(path2,list2);//////////////////////////////////////
				printf("开奖成功\n");
				getchar();
				printf("回车继续\n");
				getchar();
				break;
			case 0:
				return;
			default:
				break;
		}
	else
	{
		printf("输入错误\n"):
	}
}
		
void autoLottery(const char *path)//自动开奖
{
	flist *list=mklist();
	loadIss(path2,list2);
	iss *node=list2->pFirstNode;
	signNumber(node->id);//随机数生成中奖号码
	char *award="award";
	strcpy(node->lotterySta,award);
}

void history(const char *path2)//查看 发行历史
{
	flist *list=mklist();
	loadIss(path2,list2);
	iss *node=list2->pFirstNode;
	if(NULL==node)
	{
		systrm("clear");
		printf("暂无发行记录\n");
		getchar();
		printf("回车继续\n");
		getchar();
	}
	else
	{
		printf("期号\t中奖号码\t开奖状态\n");
		while(NULL!=node)
		{
			printf("%d\t%s\t%s\n",node->isueNum,node->winNum,node->lotterySta);
			node=node->next;
		}
	}
}
		
void findTicHis(const char *path1)//查询购票记录/////////
{
	if(NULL==path)
	{
		return;
	}
	else
	{
		blist *list1=mklist();
		loadBuy(path1,list1);
		buy *node=list1->pFirstNode;
		if(NULL==node)
		{
			printf("无购票记录\n"):
		}
		else
		{
			printf("期号:%d,id:%s,彩票号码:%s,中奖状态:%s帐号:%s\n",node->issueNum,node->id,node->winNum,node->winSta,node->name);
			node=node->next;
		}
	}
}
			
	


				



				





					






						





	
	

		



	


