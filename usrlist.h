#ifndef USR_LIST_H
#define USR_LIST_H
//#ifndef FILE_OPE
//#define FILE_OPE
#include "public.h"


#define path "./usr.info"


typedef struct usr
{
char name[20];//	char name[NAME_LEN];
char password[20];//	char password[PWD_LEN];
	double money;//余额
	
	struct usr *next;
}usr;

typedef struct List
{
	int len;
	usr *pFirstNode;
}List;

//创建用户节点
usr *mkusrNode();

//创建头节点
List *mklist();

//打开文件
FILE *openFile(const char *path,const char *mode);
/*
//保存(将用户列表保存到文件中)
void save(const char *path,const List *list);

//加载(将用户信息从文件中读取出来放入到列表中)
void load(const char *path, List *list);

//打印 
void display(const List *list);


//头插
void insert(List *list,usr *node);

//注销某个用户
void delByName(char *name,List *list);

//查找某位用户
usr *findusrByName(const char *name,const List *list);

//查找用户按余额排序
void sortusrByMoney(const char *path,List *list);
*/

//用户查看个人信息
void lookusrInfo(const char *name,const List *list);

//管理员根据用户名查看用户信息
//void lookusrInfoByName(char *path,List *list);
//void a(char *path,List *list);

//修改用户密码
void revisePwd(List *list,char *name);

//充值
void investMoney(List *list,char *name);

//注册
void regist(List *list);

//登录
usr* login(List *list);


#endif






