#ifndef _CAIPIAOLIST_H_
#define _CAIPIAOLIST_H_

#include "public.h"
//#include"buycplist.h"

#include<time.h>


typedef struct iss//发行彩票结构体
{
	int issueNum;//发布期号(要自动生成)//
	int unit_price;//彩票单价//
	char lotterySta;//开奖状态
	char winNum;//中奖号码
	int total_sold;//售出总数//
	int prize_pool;//奖池总额(要求累加)//
	
	struct iss *next;
}iss;

typedef struct flist
{
	int len;
	iss *pFirstNode;
}flist;


//创建发行彩票节点
iss *mkissNode();

//创建头节点 
flist *mkisslist();

//打开文件
FILE *openFile(const char *path,const char *mode);

//加载
void loadIss(const char *path,flist *list);

//保存
 void saveIss(const char *path,const flist *list);

//头插
void insertIss(flist *list,iss *node);

//判断发行
void judgeLottIss();

//彩票发行
void issue(flist *list);

//自动生成期号发行
void autoIssue(const char *path);

//生成彩票期号
struct tm *issueDate();

//奖池
void prize_poolAdd(const char *path,iss *node);



#endif




