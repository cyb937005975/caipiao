#include"usrlist.h"
//#include"public.h"



usr *mkusrNode()//创建用户节点
{
	usr *node=(usr*)malloc(sizeof(usr));
	if(NULL==node)
	{
		exit(EXIT_FAILURE);
	}
	memset(node, 0,sizeof(usr));
	return node;
}

List *mklist()//创建头节点
{
	List *list=(List *)malloc(sizeof(List));
	if(NULL==list)
	{
		exit(EXIT_FAILURE);
	}
	memset(list, 0,sizeof(List));
	return list;
}
/*
void insert(List *list,usr *node)//注册用户信息
{
	if(NULL==list || NULL==node)
	{
		return;
	}
	else
	{
		usr *node=mkusrNode();

		input(node);
		check(list,node);///////////
		node->next=List->pFirstNode;
		List->pFirstNode=node;
		save(PATH_NAME,list);
	}
}

void check(List *list,usr *node)//检查是否已注册
{
	if(NULL==list || NULL=node)
	{
		return;
	}
	node=List->pFirstNode;
	while(node->next==NULL)
	{
		if(usr->name=node->name)
		{
			printf("该用户名已注册\n");
		}
		node=node->next;
	}
}

void input(usr *node)//输入用户信息
{
	if(NULL==node)
	{
		return;
	}
	printf("请输入用户信息name,password,money:\n");
	scanf("%s%s%lf",node->name,node->password,&node->money);
}
*/

FILE *openFile(const char *path,const char *mode)//打开文件
{
	FILE *file=NULL;
	if(NULL!=path && NULL!=mode)
	{
		file=fopen(path,mode);
		if(NULL==file)
		{
			printf("open %s failed-> %s\n",path,strerror(errno));
			exit(EXIT_FAILURE);
		}
	}
	else
	{
		printf("参数不对..\n");
		exit(EXIT_FAILURE);
	}
	return file;
}

void save(const char *path,const List *list)//保存 
{
	if(NULL==path || NULL==list)
	{
			return;
	}
	FILE *file=openFile(path,"wb");
	usr *node=list->pFirstNode;
	while(NULL!=node)
	{
		printf("name:%s,password:%s,money:%lf\n",node->name,node->password,node->money);
		fwrite(node,sizeof(usr), 1,file);
		node=node->next;
	}
	fclose(file);
}

void load(const char *path,List *list)//加载
{
	if(NULL==path || NULL==list)
	{
			return;
	}
	FILE *file=openFile(path,"rb");
	usr *node=NULL;
	int ret=0;
	while(1)
	{
		node=mkusrNode();
		ret=fread(node,sizeof(usr), 1,file);
		if(0==ret)
		{
			free(node);
			node==NULL;
			if(feof(file))
			{
				break;
			}
			else if(ferror(file))
			{
				fclose(file);
				printf("read file error...\n");
				exit(EXIT_FAILURE);
			}
		}
		node->next=NULL;
		insert(list,node);
	}
	fclose(file);
}

void display(const List *list)//打印
{
	if(NULL==list)
	{
		return;
	}
	usr *node=list->pFirstNode;
	while(NULL!=node)
	{
		printf("name:%s,password:%s,money:%lf",node->name,node->password,node->money);
		node=node->next;
	}
}
	


void insert(List *list,usr *node)//头插
{
	if(NULL==list || NULL==node)
	{
		return;
	} 
	else
	{
		node->next=list->pFirstNode;
		list->pFirstNode=node;
	}
}
void delByName(char *name,List *list)//注销用户
{
	usr *node=mkusrNode();
//	load(path,list);
	
	usr *pre = list->pFirstNode;
	usr *cur=pre;///////////////////
	if(NULL!=list && NULL!=name)
	{
		node=list->pFirstNode;
		while(NULL!=node)
		{
			if(0==strcmp(name,node->name))
			{
				node=cur;
				if(cur=list->pFirstNode)
				{
					list->pFirstNode=cur->next;
				}
				else
				{
					pre->next=cur->next;
				}
				free(node);
				node=NULL;
				break;
			}
			node=node->next;////////////用不用保存？
		}	save(path,list);
	}
}
	
void revisePwd(List *list,char *name)//修改用户密码
{
	char password[PWD_LEN]={'\0'};
	usr *node=findusrByName(name,list);
	printf("输入新密码:\n");
	scanf("%s",password);

	strcpy(node->password,password);
	save(path,list);
}

void investMoney(List *list,char *name)//充值
{
	double add=0;
	usr *node=findusrByName(name,list);

	printf("输入要充值的金额:\n");
	scanf("%lf",&add);
	node->money=node->money+add;
	save(path,list);
}


usr *findusrByName(const char *name,const List *list)//查找某位用户
{
	usr *node=NULL;
	if(NULL!=list && NULL!=name)
	{
		node=list->pFirstNode;
		while(NULL!=node)
		{
			
			if(0==strcmp(name,node->name))
			{
				break;
			}	
			node=node->next;
		}
	}
	return node;
}

void sortusrByMoney(const char *path,List *list)//查找用户按余额排序
{
	if(NULL==list || NULL==path)
	{
		return;
	}
//	List *list=mklist();
	load(path,list);
	usr *pre=NULL;
	usr *cur=NULL;
	int i=0;
	int j=0;
	for(;i<list->len;i++)
	{
		pre=list->pFirstNode;
		cur=pre;
		for(j=0;j<list->len-1;j++)
		{
			if(cur->money > cur->next->money)
			{
				if(cur==list->pFirstNode)
				{	
					list->pFirstNode=cur->next;
					cur->next=cur->next->next;
					list->pFirstNode->next=cur;
					pre=list->pFirstNode;
					continue;
				}
				else
				{
					pre->next=cur->next;
					cur->next=cur->next->next;
					pre->next->next=cur;
					pre=pre->next;
					continue;
				}
			}
			pre=cur;
			cur=cur->next;
		}
	}
	save(path,list);
	load(path,list);
	usr *node=list->pFirstNode;
	printf("用户名\t余额\n");
	while(NULL!=node)
	{
		printf("%s\t%lf\t",node->name,node->money);
	}
}


void lookusrInfo(const char *name,const List *list)//用户查看个人信息
{
	if(NULL==list)
	{
		return;
	}
	usr *node=list->pFirstNode;
	node=findusrByName(name,list);
	printf("%s%s%lf",node->name,node->password,node->money);
}

//void lookusrInfoByName(char *path,List *list)//管理员根据用户名查看用户信息
/*void a(char *path,List *list)
{
	List *list=mklist();
	load(path,list);
	usr *node=list->pFirstNode;

	//需要做遍历，打印全部信息
//	node=findusrByName(name,list);
//	printf("%s\t%lf\t",node->name,node->money);
}
*/

void regist(List *list)//注册
{
	char name[NAME_LEN]={'\0'};
	printf("输入注册用户名:\n");
	scanf("%s",name);
	if(NULL!=findusrByName(name,list))
	{
		printf("注册失败-->该用户名已存在\n");
	}
	else
	{
		char password[PWD_LEN]={'\0'};
		printf("输入注册密码:\n");
		scanf("%s",password);


		usr *node=mkusrNode();//创建新节点
		strcpy(node->name,name);
		strcpy(node->password,password);
		insert(list,node);
		save(path,list);
	}
}
		

usr* login(List *list)//登录
{
	usr *node=NULL;
	if(NULL!=list)
	{
	char name[NAME_LEN]={'\0'};//初始化
		printf("输入用户名:\n");
		scanf("%s",name);//////////////////////
		node=findusrByName(name,list);
		if(NULL==node)
		{
			printf("登录失败-->用户名不存在!\n");
		}
		else
		{
			char password[PWD_LEN]={'\0'};
			printf("输入用户密码:\n");
			scanf("%s",password);///////////////////
			if(0==strcmp(password,node->password))
			{
				printf("登录成功\n");
			}
			else
			{
				printf("登录失败-->密码错误\n");
				node=NULL;
			}
		}
	}
	else
	{
		printf("参数有误\n");
	}

	return node;
}






































		

