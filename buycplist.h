#ifndef _BUYCPLIST_H
#define _BUYCPLIST_H

#include"public.h"
#include"usrlist.h"
#include"caipiaolist.h"


typedef struct buy//个人购买彩票结构体
{
	int issueNum;//发布期号
	char id[32];//彩票唯一标识id
	char winNum[32];//彩票选中号码
	char name[32];//购买者帐号
	int total_buy;//购买注数
	char winSta[32];//中奖状态
	double prize_win;//中奖金额

	struct buy *next;
}buy;

typedef struct blist
{
	int len;
	buy *pFirstNode;
}blist;

//创建购买彩票节点
buy *mkbuyNode();

//创建头节点
blist *mkbuylist();

//打开文件
FILE *openfile(const char *path,const char *mode);

//加载
void loadBuy(const char *path,blist *list);

//保存
void saveBuy(const char *path,const blist *list);

//头插
void insertBuy(blist *list,buy *node);

//买票
void buyTic(const char *path,const char *path1,const char *path2);

//随机数
void signNumber(char a[]);

//查询中奖记录
void findwinJl(const char *path);

//开奖
void lottery(const char *path1,const char *path2);

//自动开奖
void autoLottery(const char *path);

//查看历史发行
void history(const char *path2);

//查询购票记录
void findTicHis(const char *path1);

#endif



