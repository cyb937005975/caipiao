#ifndef _JIEMIAN_H_
#define _JIEMIAN_H_

#include"public.h"
#include"usrlist.h"
//创建入口
int rukou();

//用户管理员界面选择
int choose();

//选择进入
void switchIn(List *list);

//用户界面
int usrView();

//选择用户界面功能
void switchByUsrView(List *list,char *name);

//管理员界面
int adminView();

//选择管理员界面功能
void switchByAdminView(List *list);

//公证员界面
int workerView();

//选择公证员界面功能
void switchByWorkerView();
	



#endif
